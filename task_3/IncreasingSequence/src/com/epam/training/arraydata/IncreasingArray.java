package com.epam.training.arraydata;
import java.util.Scanner;

public class IncreasingArray {
	
	public  int [] takeArrayFromConsole ( ){
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter size of array:");
		
		int count = scan.nextInt();
		System.out.println("Enter "+ count + " integer(s) for array:");
		scan.nextLine();
	
		int [] numbers = new int [count];
		
		Scanner scnum = new Scanner (scan.nextLine());
		
		for (int i = 0; i < count; i++) {
			if (scnum.hasNextInt()){
				numbers [i] = scnum.nextInt();
			}
			else{
				numbers = null;	
			}
		
		}
		return 	numbers ;
		
	}
	
	public static int checkArrayIncrease(int[] mas){
		
		int len = mas.length;
		int flag = 0;
		for (int i = 0; i < len-1;) {
			if ( mas[i] < mas[i+1]){
				i++;
			}
			else{break;}
			if(i == len-1){
				flag =1;
			}
			
		}
		return flag;
	}

}
