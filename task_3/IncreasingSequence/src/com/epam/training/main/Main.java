package com.epam.training.main;

import com.epam.training.report.Report;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
	
	 static Logger logger = LogManager.getLogger("Main");

	public static void main(String[] args) {
		
		logger.log(Level.INFO, "this first log");

		Report rep = new Report("Test report","Guest");
		System.out.println("Report name: " + rep.getReportName());
		System.out.println("Creator name: " + rep.getCreatorName());
		rep.generateReport();

	}

}

