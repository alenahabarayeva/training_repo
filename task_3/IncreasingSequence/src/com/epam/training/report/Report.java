package com.epam.training.report;

import com.epam.training.arraydata.IncreasingArray;


public class Report {
	
	private String reportName;
	private String creatorName;
	
	public Report(){
		reportName = "Default name";
		creatorName = "Guest";
	}
	
	public Report(String rep, String creat){
		reportName = rep;
		creatorName = creat;
	}
	
	public String getReportName(){
		return reportName;
	}
	
	public String getCreatorName(){
		return creatorName;
	}
	
	public void setReportName( String rep){
		this.reportName = rep;
	}
	
	public void setcreatorName(String cren){
		this.creatorName = cren ;
	}
	
	
	public void generateReport() {
		
			
		IncreasingArray iarr = new IncreasingArray();
		
		int mas[] = iarr.takeArrayFromConsole();
		
		if (mas != null ){
			
			int a = IncreasingArray.checkArrayIncrease(mas);
			
			if (a == 1){
				System.out.println("Sequence is increasing.");
			}
			
			else{
				System.out.println("Sequence is not increasing.");
			}
		}
		
		else{
			System.out.println("You didn't provide enough numbers or you entered not only integers. Please retype.");
		}
		
				
	}
	
}
