package test.com.epam.training.arraydata;

import com.epam.training.arraydata.IncreasingArray;
import org.junit.Assert;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import org.junit.AfterClass;

public class IncreasingSequenceTest {
	
	private IncreasingArray arr;
	public static int count = 0;


	@Before	 
	 public void initIncreasingArray() {
		arr = new IncreasingArray();
		
	}
	
	@After 
	 public void clearIncreasingArray() {
		arr = null;
	}
	
	 @AfterClass
     public static void afterTest(){
		 System.out.println("Count of tests: " + count);
         count = 0;
	 }
	 
	 
	@Test
	public void checkArrayIncreaseTest(){
		int[] num = {1,2,3};
		Assert.assertEquals(1, IncreasingArray.checkArrayIncrease(num));
		count++;
	}
	
	@Test
	public void takeArrayFromConsoleTestIS(){
		System.out.println("Enter increasing sequence");
		Assert.assertEquals(1, IncreasingArray.checkArrayIncrease(arr.takeArrayFromConsole()));
		count++;
	}
	
	@Test
	public void takeArrayFromConsoleTestNIS(){
		System.out.println("Enter not increasing sequence");
		Assert.assertEquals(0, IncreasingArray.checkArrayIncrease(arr.takeArrayFromConsole()));
		count++;
	}

	@Test
	public void takeArrayFromConsoleNullTest(){
		Assert.assertNull(arr.takeArrayFromConsole());
		count++;
	}

}
