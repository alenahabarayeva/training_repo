package com.epam.training.parsing;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.epam.training.entity.TextLeaf;
import com.epam.training.exception.ParseException;

public class SymbolParser extends AbstractTextParser{
	
	public static final String REGEX_SYMBOL = ".{1}";
	 
	@Override
	 public void parse (String text) throws ParseException{
		
			 TextLeaf sym = new TextLeaf();
			 Pattern symPattern = Pattern.compile(REGEX_SYMBOL);
			 Matcher symMatcher = symPattern.matcher(text);
			 String symb = "";
			 while(symMatcher.find()){
				 symb = symMatcher.group();
				 sym = new TextLeaf(symb);
				 
				
				 getTextComponent().addElement(sym);
				// System.out.println(symb);
			 }
			 
		 
	}

}
