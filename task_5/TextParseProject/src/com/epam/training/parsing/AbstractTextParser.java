package com.epam.training.parsing;

import com.epam.training.entity.ITextComponent;
import com.epam.training.entity.TextComposite;
import com.epam.training.exception.ParseException;


public abstract class AbstractTextParser {
	
	protected AbstractTextParser successor;
	private ITextComponent textComponent = new TextComposite();
	
	public AbstractTextParser getSuccessor (AbstractTextParser suc){
		return this.successor;
	}
	
	public void setHandler(AbstractTextParser absHand)
	{
		this.successor = absHand;
	}
	
	public AbstractTextParser getSuccessor (){
		return this.successor;
	}
	
	public ITextComponent getTextComponent (){
		return this.textComponent;
	}	
	
	public void clearComponent()
	{
		textComponent = new TextComposite();
	}
	
	
	public abstract void parse(String text) throws ParseException;

}
