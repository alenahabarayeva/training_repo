package com.epam.training.parsing;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.epam.training.exception.ParseException;

public class WordSignParser extends AbstractTextParser{

	public static final String REGEX_WORD_AND_SIGN = "([\\.,!\\?:;@]{1})|([^\\.,!\\?:;@]*)";
	 
	@Override
	 public void parse (String text) throws ParseException{
		 if(getSuccessor() != null){
			 Pattern wsPattern = Pattern.compile(REGEX_WORD_AND_SIGN);
			 Matcher wsMatch = wsPattern.matcher(text);
			 String wordSign = "";
			 while(wsMatch.find()){
				 wordSign = wsMatch.group();
				// System.out.println(wordSign);
				 successor.parse(wordSign);
				 getTextComponent().addElement(successor.getTextComponent());
				 successor.clearComponent();
			 }
		 }else {
			 throw new ParseException();
		 }
	}
}
