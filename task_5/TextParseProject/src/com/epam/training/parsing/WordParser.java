package com.epam.training.parsing;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.epam.training.exception.ParseException;

public class WordParser extends AbstractTextParser{
	
	public static final String REGEX_WORD = "([^(\\s)]*)(\\s)*";
	
	@Override
	 public void parse (String text) throws ParseException{
		 if(getSuccessor() != null){
			 
			 Pattern wordPatern = Pattern.compile(REGEX_WORD);
			 Matcher wordMatch = wordPatern.matcher(text);
			 String word = "";
			 while (wordMatch.find()){
				 word = wordMatch.group();
				// System.out.println(word);
				 successor.parse(word);
				 getTextComponent().addElement(successor.getTextComponent());
				 successor.clearComponent();
			 
			 }
		 }else{
			 throw new ParseException();
		 }
	}		 
			 

}
