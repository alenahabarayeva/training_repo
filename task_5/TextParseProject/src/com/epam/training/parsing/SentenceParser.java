package com.epam.training.parsing;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.epam.training.exception.ParseException;

public class SentenceParser extends AbstractTextParser{
	
	 public static final String REGEX_SENTENCE = "([^(\\.|!|\\?)]+)(\\.|!|\\?)";
	 
	 @Override
	 public void parse (String text) throws ParseException{
		 if(getSuccessor() != null){
			 
			 Pattern senPattern = Pattern.compile(REGEX_SENTENCE);
			 Matcher senMatch = senPattern.matcher(text);
			 String sentence = "";
			 while(senMatch.find()){
				 sentence = senMatch.group();
				// System.out.println(sentence);
				 //count++;System.out.println(count);
				 successor.parse(sentence);
				 getTextComponent().addElement(successor.getTextComponent());
				 successor.clearComponent();
			 
			 }
		 }else{
			 throw new ParseException();
		 }

	 }
}
