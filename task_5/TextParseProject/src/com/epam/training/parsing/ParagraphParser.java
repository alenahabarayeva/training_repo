package com.epam.training.parsing;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.epam.training.entity.TextLeaf;
import com.epam.training.exception.ParseException;

public class ParagraphParser extends AbstractTextParser {
	
	public static final String REGEX_LISTING = "\\s*(Start listing)([^\\t]+)(End listing)";
	public static final String REGEX_PARAGRAPH_WITH_LISTING = "(\\s*(.+))([^(\\s*(Start listing)([^\\t]+)(End listing)\\s)])|\\s*(Start listing)([^\\t]+)(End listing)";
	
	
	@Override
	public void parse (String text) throws ParseException{
		
		if(getSuccessor() != null){
			
			TextLeaf paragraphLeaf = new TextLeaf();
		    String paragraph = "";
			Pattern parPattern = Pattern.compile(REGEX_PARAGRAPH_WITH_LISTING);
			Matcher match = parPattern.matcher(text);
			while (match.find()){
				paragraph = match.group();
			
				//System.out.print(paragraph);
				 if (Pattern.matches(REGEX_LISTING, paragraph)) {
		                // if listing - add to list without parsing
		                paragraphLeaf = new TextLeaf(paragraph);
		                getTextComponent().addElement(paragraphLeaf);
		         } else {
		        	 successor.parse(paragraph);
		        	 getTextComponent().addElement(successor.getTextComponent());
		        	 getTextComponent().addElement(new TextLeaf("\n"));
		        	 successor.clearComponent();
		           }
			}
			
		}else{
			
			throw new ParseException();
			
		}
				
	 }
}
	
	

