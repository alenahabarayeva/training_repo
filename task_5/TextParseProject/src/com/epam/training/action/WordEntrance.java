package com.epam.training.action;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.training.entity.ITextComponent;

public class WordEntrance {
	
	static Logger log = LogManager.getLogger("WordEntrance");
	
	public HashMap <String, Integer> findWordEntrance (ITextComponent sourceText, List <String> wordList){
		log.log(Level.DEBUG, "Start counting entrances for words "+ wordList.toString());
		HashMap<String, Integer> paragraphlMap = new HashMap<String, Integer>();
		int parNumber = 0;
		for(ITextComponent source : sourceText.getTextList()){
			String str = source.toString();
			if (str != "\n"){
				int count = 0;
				parNumber++;
				
				String paragNum = null;
				
				for(String word : wordList){
					Pattern p = Pattern.compile(word);
				    Matcher m = p.matcher(str);
				    
				    while(m.find()) 
				    	count++;
				    
				}
				paragNum = "Paragraph �"+parNumber;
				paragraphlMap.put(paragNum, count);
				log.log(Level.INFO, paragNum + " has " + count + " number(s) of entrances");
				
			}
			
			
		}
		return paragraphlMap;
		
	}
	
	public static <K,V extends Comparable<? super V>>
	SortedSet<Map.Entry<K,V>> entriesSortedByValues(Map<K,V> map) {
		log.log(Level.INFO, "Start sorting paragraphs in order by counts of entrances.");
	    SortedSet<Map.Entry<K,V>> sortedEntries = new TreeSet<Map.Entry<K,V>>(
	        new Comparator<Map.Entry<K,V>>() {
	            @Override public int compare(Map.Entry<K,V> e1, Map.Entry<K,V> e2) {
	                int res = e2.getValue().compareTo(e1.getValue());
	                return res != 0 ? res : 1;
	            }
	        }
	    );
	    sortedEntries.addAll(map.entrySet());
	    return sortedEntries;
	}

}
