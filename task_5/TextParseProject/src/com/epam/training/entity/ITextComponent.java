package com.epam.training.entity;

import java.util.List;

public interface ITextComponent {
	
	public void addElement(ITextComponent t);
	public void removeElement (ITextComponent t);
	public Object getChild (int index);
	public List <ITextComponent> getTextList();
	public String toString();
}
