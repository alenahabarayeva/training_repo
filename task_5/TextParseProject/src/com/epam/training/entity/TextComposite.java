package com.epam.training.entity;

import java.util.ArrayList;
import java.util.List;

public class TextComposite implements ITextComponent{
	
	private List<ITextComponent> text = new ArrayList<ITextComponent>();
	
	public TextComposite (List<ITextComponent> t){
		this.text = t;
	}
	
	public TextComposite (){
		
	}
	
	@Override
	public void addElement (ITextComponent t){
		text.add(t);
	}
	
	public void removeElement (ITextComponent t){
		text.remove(t);
	}
	
	public ITextComponent getChild (int i) {
		return text.get(i);
	}
	
	public List<ITextComponent>  getTextList () {
		return text;
	}
	
	public String toString(){
		
		StringBuilder sb = new StringBuilder();
		for (ITextComponent tc : text){
			sb.append(tc.toString());
		}
		return sb.toString();
	}


}
