package com.epam.training.entity;

import java.util.List;

public class TextLeaf implements ITextComponent{
	
	private String str;
	
	public TextLeaf (String s){
		this.str = s;
	}
	
	
	public TextLeaf() {
		// TODO Auto-generated constructor stub
	}



	public void addElement (ITextComponent t){
		throw new UnsupportedOperationException();
	}
	
	public void removeElement(ITextComponent t){
		throw new UnsupportedOperationException();
	}
	
	public ITextComponent getChild (int i){
		return this;
	}
	
	public String toString(){
		return  str ;
		
	}
	
	public List<ITextComponent>  getTextList () {
		throw new UnsupportedOperationException();
	}

}
