package com.epam.training.file;



import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TextReader {
	
	static Logger log = LogManager.getLogger("TextReader");
	
	public String readText(String sourceFile){
		
		StringBuilder strBuilder = new StringBuilder();
		List<String> text = new ArrayList<String>();
		
		try{
			
			text = Files.readAllLines(Paths.get(sourceFile));
			for(String line : text){
				strBuilder.append(line);
				strBuilder.append("\n");
				
			}
			
			
			
		}
		catch(FileNotFoundException ex) {
			System.err.println(
            "Unable to open file '" + 
            		sourceFile + "'");  
			 log.log(Level.ERROR,"Can't open file: ", ex);
		}
		catch(IOException ex) {
			System.err.println(
            "Error reading file '" 
            + sourceFile + "'");  
			log.log(Level.ERROR,"Can't read file: ", ex);
        
		}
		return strBuilder.toString();
		
	}

}
