package test.com.training.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.epam.training.action.WordEntrance;
import com.epam.training.exception.ParseException;
import com.epam.training.file.TextReader;
import com.epam.training.parsing.ParagraphParser;
import com.epam.training.parsing.SentenceParser;
import com.epam.training.parsing.SymbolParser;
import com.epam.training.parsing.WordParser;
import com.epam.training.parsing.WordSignParser;
import static com.epam.training.action.WordEntrance.entriesSortedByValues;

public class WordEntranceTest {
	
	
	
	private ParagraphParser paragParser;
	private SentenceParser sentParser;
	private WordParser wordParser;
	private WordSignParser wordSParser;
	private SymbolParser symParser;
	private List<String> findingWords;
	private String sourceFile = "data/sourceText.txt";
	
	
	@Before
	public void initVariable (){
		
		paragParser = new ParagraphParser();
		sentParser = new SentenceParser();
		wordParser = new WordParser();
		wordSParser = new WordSignParser();
		symParser = new SymbolParser();
		
		paragParser.setHandler(sentParser);
		sentParser.setHandler(wordParser);
		wordParser.setHandler(wordSParser);
		wordSParser.setHandler(symParser);
		
		findingWords = new ArrayList<String>();
		
	}
	
	@After
	public void nullVariable (){
		
		paragParser = null;
		sentParser = null;
		wordParser = null;
		wordSParser = null;
		symParser = null;
		findingWords = null;
		
	}
	
	
	public int findMax (Collection <Integer> intList){
		int a = 0;
		for (int i : intList){
			if (i > a){
				a = i;
			}
		}
		return a;
	}
	
	
	@Test
	public void findWordEntrance(){
		try{
			findingWords.add("Finally");
			
			TextReader sourceText = new TextReader();
			
			paragParser.parse(sourceText.readText(sourceFile));
			WordEntrance testWE = new WordEntrance();
			HashMap  <String, Integer> testHM = testWE.findWordEntrance(paragParser.getTextComponent(),findingWords);
			int actual = findMax( testHM.values());
			Assert.assertEquals(actual, 1, 0.001);
		}
		catch(ParseException e) {
			Assert.fail();
		}
		
		findingWords.clear();
	}
	
	@Test
	public void entriesSortedByValueTest(){
		try{
			findingWords.add("Finally");
			findingWords.add("climbed");
			findingWords.add("puppy");
			findingWords.add("is");
			TextReader sourceText = new TextReader();
			
			paragParser.parse(sourceText.readText(sourceFile));
			WordEntrance testWE = new WordEntrance();
			HashMap  <String, Integer> testHM = testWE.findWordEntrance(paragParser.getTextComponent(),findingWords);
			String actual = entriesSortedByValues(testHM).toString();
			String expected = "[Paragraph �1=4, Paragraph �7=3, Paragraph �8=3, Paragraph �2=3, Paragraph �6=2, Paragraph �5=1, Paragraph �4=1, Paragraph �3=1]";
			Assert.assertEquals(expected, actual);
		}
		catch(ParseException e) {
			Assert.fail();
		}
		
		findingWords.clear();
		
	}

}
