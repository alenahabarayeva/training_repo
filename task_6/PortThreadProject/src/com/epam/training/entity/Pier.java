package com.epam.training.entity;


public class Pier {
	
	private String pierName;
	public Pier (String p){
		this.pierName = p;
	}
	
	public String getPierName(){
		return this.pierName;
	}
	
	public int containerExcange (PortAventure port, int x, int loadCont, int shipCapacity){
		int shipCnt = loadCont;
		try{
			
			String s = this.pierName+ " is " + ((x == 0) ? "adding container on port. " : (x == 1) ? "getting container from port." 
						: "exchange container from port and ship. ") ;
			s += "Initial status";
            s += ": port contains - " + port.containersInPort();
            s += ", ship contains - " + shipCnt;
            
            switch (x) {
            case 0:
                if (shipCnt > 0)	
                    if (port.addContainer()) {
                    	shipCnt--;
                       
                    }
                break;
            case 1:
                if (shipCnt < shipCapacity)
                    if (port.getContainer()) {
                    	shipCnt++;
                    }
                        break;
                        
            case 2:
                if (shipCnt > 0)
                    if (port.addContainer()) {
                    	shipCnt--;
                    }

                try {
                	Thread.sleep(new java.util.Random().nextInt(10));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if (shipCnt < shipCapacity)
                    if (port.getContainer()) {
                    	shipCnt++;
                    }
                break;            
            }
            s += ". Final status: port contains - " + port.containersInPort();
            s += ", ship contains - " + shipCnt + " containers.";
            System.out.println(s);
            
		
			Thread.sleep(new java.util.Random().nextInt(10));
			
		} catch (InterruptedException e){
			e.printStackTrace();
		}
		return shipCnt;
	}

}
