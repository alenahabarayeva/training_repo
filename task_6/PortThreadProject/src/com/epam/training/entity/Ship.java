package com.epam.training.entity;

import java.util.Random;

import com.epam.training.exception.PierResourceException;

public class Ship extends Thread{
	private boolean mooring;
	private String shipName;
	private int loadedContainers;
	private int shipCapacity;
	private PortAventure port;
	
	public Ship(PortAventure p, String name, int loadCont, int capac){
		this.port = p;
		this.shipName = name;
		this.loadedContainers = loadCont;
		this.shipCapacity = capac;
	}
	
	public int getShipContainers(){
		return this.loadedContainers;
	}
	
	
	public void setShipContainers(int cont){
		this.loadedContainers = cont;
	}
	
	
	
	public void run(){
		Pier freePier = null;
		try{
			freePier = port.getResource(1);
			mooring = true;
			System.out.println("Ship "+ this.shipName + 
					" is mooring on the pier "
			+ freePier.getPierName()) ;
			int x = new Random().nextInt(3);
			this.loadedContainers = freePier.containerExcange(this.port, x, this.loadedContainers, this.shipCapacity);
			
		
		} catch (PierResourceException e){
			System.out.println("No free piers now."+e.getMessage());
		}finally{
			if(freePier != null){
				System.out.println("Ship "+this.shipName+" left pier "+freePier.getPierName());
				port.returnResource(freePier);
			}
		}
	}
	public boolean isMooring(){
		return mooring;
	}

}
