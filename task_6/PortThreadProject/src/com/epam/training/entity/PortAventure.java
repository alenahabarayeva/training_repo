package com.epam.training.entity;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

import com.epam.training.exception.PierResourceException;

public class PortAventure {
	
	private static PortAventure instance = null; 
	private static ReentrantLock lock = new ReentrantLock(); 
	private PortAventure() { } 
	//private final static int PORT_SIZE = 5;
	
	private final static ArrayBlockingQueue<Pier> piers = new ArrayBlockingQueue<Pier>(5);
	
	//);
//	private final Semaphore portSemph = new Semaphore(PORT_SIZE, true);
	private static AtomicInteger containers = new AtomicInteger();
    private int portCapacity = 10;
    public static PortAventure getInstance() {
    	
    	lock.lock(); // блокировка   
    	try { 
    		   if (instance == null) { 
    			  
    			   piers.add(new Pier("Pier1"));
    			   piers.add(new Pier("Pier2"));
    			   piers.add(new Pier("Pier3"));
    			   piers.add(new Pier("Pier4"));
    			   piers.add(new Pier("Pier5"));
    			  
    			   containers.set(5);
    			   instance = new PortAventure();   
    		   } 
    	
    	} finally { 
    		lock.unlock(); // снятие блокировки 
    	}
    	return instance;
    	}
    
	

	public Pier getResource(long maxWaitMillis) throws PierResourceException{
		
				try{
				Pier res = piers.take();
				
				return res;
				}
				catch (InterruptedException e) {
					throw new PierResourceException(e);
					
				}
			
		}
		
	
	public boolean addContainer() {
        if (containers.get() < portCapacity) {
            containers.incrementAndGet();
            return true;
        }
        else
            return false;
    }
	
	public boolean getContainer() {
        if (containers.get() > 0) {
            containers.decrementAndGet();
            return true;
        }
        else
            return false;
    }
	
	 public int containersInPort() {
	        return containers.get();
	    }

	
	public void returnResource(Pier res){
		piers.add(res);
	//	portSemph.release();
	}

	
}
