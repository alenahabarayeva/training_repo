package com.epam.training.file;

import java.util.ArrayList;
import java.util.List;

import com.epam.training.entity.PortAventure;
import com.epam.training.entity.Ship;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;





public class ShipCreatorFromFile {
	
	static Logger logger = LogManager.getLogger("ShipCreatorFromFile");
	public List<Ship> createShip(List<ArrayList<String>> fileData) {
		
		List<Ship> shipList = new ArrayList<Ship> ();
		logger.log(Level.INFO, "Load data from file.");


		for (int i = 0; i<fileData.size(); i++){
			ArrayList<String> constr = new ArrayList<String> (fileData.get(i));
			
			try{
				Integer lc = Integer.valueOf(constr.get(2));
			
				int cap = Integer.parseInt(constr.get(3));
				shipList.add(new Ship(PortAventure.getInstance(),constr.get(1), lc ,cap));
				
						;
			}catch(NumberFormatException | IndexOutOfBoundsException e){
				
				logger.log(Level.ERROR, "Incorrect input values in row, which number : "+(i+1), e);
				
			}
			
		}
		
		return shipList;
	}

}
