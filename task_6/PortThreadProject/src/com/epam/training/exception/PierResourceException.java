package com.epam.training.exception;

public class PierResourceException extends Exception{
	public PierResourceException(){
		super();
	}
	
	public PierResourceException(String message, Throwable cause){
		super(message, cause);
		
	}
	
	public PierResourceException (String message){
		super(message);
	}
	
	public PierResourceException (Throwable cause){
		super(cause);
	}

}
