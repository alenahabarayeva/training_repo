package com.epam.training.main;

import java.util.ArrayList;

import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.training.entity.Ship;
import static com.epam.training.file.DataReaderFromFile.readFile;
import com.epam.training.file.ShipCreatorFromFile;

public class Main {
	public static void main(String[] args) {
		
		Logger logger = LogManager.getLogger("Main");
		String path = "data/ship.txt";
		logger.log(Level.INFO, "Start thread process.");
		ShipCreatorFromFile creator = new ShipCreatorFromFile();
		List <Ship>  ships = new ArrayList<Ship>();
		ships = creator.createShip(readFile(path));
	
		for(Ship sh : ships){
			sh.start();
		}
		
	}

}
