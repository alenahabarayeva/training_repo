package com.epam.training.creator;

import java.util.ArrayList;
import java.util.List;

import com.epam.training.sweetness.AbstractSweetness;
import com.epam.training.type.SweetType;

public class PresentGenerator {
	
	public List<AbstractSweetness> generatePresent(int sweetCount){
		List<AbstractSweetness>present = new ArrayList<AbstractSweetness>();
		
		for (int i=0; i<sweetCount; i++){
			SweetType st = SweetType.getRandom();
			present.add(i,SweetnessFactory.getSweetnessFromFactory(st));
		
			
		}
		return present;
	}

}
