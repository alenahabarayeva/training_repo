package com.epam.training.creator;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.training.sweetness.AbstractSweetness;
import com.epam.training.sweetness.Candy;
import com.epam.training.type.CandyMass;
import com.epam.training.type.SweetFillerType;



public class CandyCreatorFromFile {
	
	static Logger logger = LogManager.getLogger("CandyCreatorFromFile");
	public List<AbstractSweetness> createCandy(List<ArrayList<String>> fileData) {
		
		List<AbstractSweetness> candyList = new ArrayList<AbstractSweetness> ();
		logger.log(Level.INFO, "Load data from file.");

		for (int i = 0; i<fileData.size(); i++){
			ArrayList<String> constr = new ArrayList<String> (fileData.get(i));
			try{
				double w = Double.parseDouble(constr.get(1));
				double s = Double.parseDouble(constr.get(2));
				candyList.add(new Candy(constr.get(0),w ,s,
						SweetFillerType.convert(constr.get(3)),
						CandyMass.convert(constr.get(4))));
			}catch(NumberFormatException | IndexOutOfBoundsException e){
				
				logger.log(Level.ERROR, "Incorrect input values in row, which number : "+(i+1), e);
				
			}
			
		}
		
		return candyList;
	}

}
