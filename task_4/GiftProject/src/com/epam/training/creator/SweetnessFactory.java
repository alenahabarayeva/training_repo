package com.epam.training.creator;

import com.epam.training.sweetness.AbstractSweetness;
import com.epam.training.sweetness.Candy;
import com.epam.training.sweetness.Chocolate;
import com.epam.training.sweetness.Wafer;
import com.epam.training.type.CandyMass;
import com.epam.training.type.SweetFillerType;
import com.epam.training.type.SweetType;



public class SweetnessFactory {
	public static AbstractSweetness getSweetnessFromFactory (SweetType st){
		switch(st){
		case CN_NUT:
			return new Candy("Candy Nut", 15.5, 2.3, SweetFillerType.DARKCHOCOLATE, CandyMass.getRandom());
		case CN_CR_CHOCO:
			return new Candy("Crazy Choco", 14.3, 2.0,SweetFillerType.MILKCHOCOLATE, CandyMass.getRandom());
		case CR_HERBINA:
			return new Candy("Caramel Herbina", 10.3, 1.5,SweetFillerType.GLAZECARAMEL,  CandyMass.getRandom());
		case CR_DUSHESS:
			return new Candy("Caramel Dushess", 10.2, 1.2,SweetFillerType.CARAMEL,  CandyMass.getRandom());
		case CH_SLIVKI:
			return new Chocolate("Chocolate Slivki-Lenivki", 50, 5, SweetFillerType.DARKCHOCOLATE);
		case CH_ELEGANCE:
			return new Chocolate("Chocolate Elegance", 25, 3, SweetFillerType.MILKCHOCOLATE);
		case WF_ROSHETTO:
			return new Wafer("Wafer Roshetto", 20, 4.5, SweetFillerType.CONDENSEDMILK);
		case WF_KONAFETTO:
			return new Wafer("Wafer Konafetto", 20, 4.8, SweetFillerType.CONDENSEDMILK);	
				
		default: 
			System.out.println("wrong");
			throw new IllegalArgumentException("illegal mode");
		}
		
	}

}
