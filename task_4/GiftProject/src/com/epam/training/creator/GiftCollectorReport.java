package com.epam.training.creator;

import static com.epam.training.action.PresentAction.countPresentWeight;
import static com.epam.training.action.PresentAction.findSweet;

import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.training.creator.PresentGenerator;
import com.epam.training.sweetness.AbstractSweetness;
import com.epam.training.sweetness.Present;

public class GiftCollectorReport {
	
	static Logger log = LogManager.getLogger("PresentAction");
	
	private String repName;
	private String presentName;
	private int sweetCount;
	private double startPoint;
	private double endPoint;
	
	
	public GiftCollectorReport (String rn, String pn, int c, double sp, double ep){
		this.repName = rn;
		this.presentName = pn;
		this.sweetCount = c;
		this.startPoint = sp;
		this.endPoint = ep;
	}
	
	public void generateGiftReport(){
		
		log.log(Level.INFO,"Generate report of creating present.");
		System.out.println("Report: "+repName);
		//Generate sweets
		PresentGenerator gen = new PresentGenerator();
		
		//create present
		Present present = new Present(this.presentName);
		System.out.println("Present has name: "+presentName);
		
		//put sweets into present
		present.setPresentSweetList(gen.generatePresent(this.sweetCount));
		System.out.println("Present contains: "+sweetCount+" number(s) of sweets. List of sorted sweets you can see below:");
		present.getPresentSweetList().forEach(AbstractSweetness::typeSweet);
		
		//count presents weight
		double w = countPresentWeight(present.getPresentSweetList());
		System.out.println("Presents weight is equal " + w);
		
		//find sweets for a given sugar range
		System.out.println("Finding sweets with sugar range from "+startPoint+" till "+endPoint);
		List<AbstractSweetness> range = findSweet(present.getPresentSweetList(), startPoint, endPoint);
		if (range.size()>0){
		System.out.println("Sweet(s) with necessary sugar: ");
		range.forEach(AbstractSweetness::typeSweet);
		}else{
		System.out.println("No sweets are found in sugar range.");
		}
	 
		
	}
	
	

}
