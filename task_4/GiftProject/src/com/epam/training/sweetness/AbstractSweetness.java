package com.epam.training.sweetness;

public abstract class AbstractSweetness {
	
	private String name;
	private double weight;
	private double sugarCont;
	
	public AbstractSweetness(String n, double w, double s){
		this.name = n;
		this.weight = w;
		this.sugarCont = s;
	}
	
	
	public abstract void typeSweet();
	
	public String getName(){
		return name;
	};
	
	public double getWeight(){
		return weight;
	};
	
	public double getSugarCont(){
		return sugarCont;
	};

}
