package com.epam.training.sweetness;

import com.epam.training.type.SweetFillerType;

public class Chocolate extends AbstractSweetness{
	private SweetFillerType chocoType;
	
	public Chocolate(String n, double w, double s, SweetFillerType cht){
		super(n, w, s);
		this.chocoType = cht;
	}
	
	@Override
	public void typeSweet(){
		System.out.println("Chocolate name is "+ super.getName()+". Chocolate weight is "+ super.getWeight()+"gr. Chocolate contains "+super.getSugarCont()+"gr of sugar. Chocolate has type "+chocoType);
	}
	
	@Override
	public double getWeight(){
		return super.getWeight();
	}

}
