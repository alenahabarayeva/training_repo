package com.epam.training.sweetness;


import java.util.List;

public class Present {
	
	private String name;
	private List<AbstractSweetness> sweetList;
	
	public Present (String n){
		this.name = n;	
	}
	
	public String getPresentName(){
		return this.name;
	}
	
	
	public List<AbstractSweetness> getPresentSweetList(){
		return this.sweetList;
	}	
	
	public void setPresentSweetList(List<AbstractSweetness> sl){
		this.sweetList = sl;
	}		
}


