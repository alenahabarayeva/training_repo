package com.epam.training.sweetness;

import com.epam.training.type.SweetFillerType;

public class Wafer extends AbstractSweetness{
	
	private SweetFillerType filler;
	
	public Wafer(String n, double w, double s, SweetFillerType fil){
		super(n, w, s);
		this.filler = fil;
		
	}
	
	@Override
	public void typeSweet(){
		System.out.println("Wafer name is "+ super.getName()+". Wafer weight is "+ super.getWeight()+"gr. Wafer contains "+super.getSugarCont()+"gr of sugar. Wafer has filler "+filler);
	}
	
	@Override
	public double getWeight(){
		return super.getWeight();
	}

}
