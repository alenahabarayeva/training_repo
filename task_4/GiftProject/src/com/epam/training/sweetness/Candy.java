package com.epam.training.sweetness;

import com.epam.training.type.CandyMass;
import com.epam.training.type.SweetFillerType;

public class Candy extends AbstractSweetness{
	
	private SweetFillerType candyType;
	private CandyMass massType;

	public Candy(String n, double w, double s, SweetFillerType t, CandyMass cm){
					
			super(n,w,s);
			this.candyType = t;
			this.massType = cm;
	}
	
	
	@Override
	public void typeSweet(){
		System.out.println("Candy name is "+ super.getName()+". Candy weight is "+ super.getWeight()+"gr. Candy contains "+super.getSugarCont()+"gr of sugar. Candy type is "+this.candyType+
				". Candy mass is "+this.massType);
	}
	
	@Override
	public double getWeight(){
		return super.getWeight();
	}

}
