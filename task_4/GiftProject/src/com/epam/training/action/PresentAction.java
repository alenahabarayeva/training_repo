package com.epam.training.action;

import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Comparator;
import com.epam.training.sweetness.AbstractSweetness;

public  class PresentAction {
	
	static Logger log = LogManager.getLogger("PresentAction");
	
	public static List<AbstractSweetness> sortPresent (List<AbstractSweetness> sweetList){
		
		log.log(Level.INFO, "Start sorting present");
		sweetList.sort(Comparator.comparing(AbstractSweetness::getName).thenComparing(AbstractSweetness::getWeight));
		//System.out.println("Present is sorted. See the Sorted present below.");
		sweetList.forEach(AbstractSweetness::typeSweet); 
		return sweetList;
		

	}
		
	public static double countPresentWeight(List<AbstractSweetness> sweetList) {
		
		double w =0;
		for (int i=0; i<sweetList.size(); i++){
			w += sweetList.get(i).getWeight();
		}
		log.log(Level.INFO, "Present has weight, which is equal "+ w);
		//System.out.println("Present weight is " + w + " gr.");
		return w;
		
	}
	
	
	
	public static List<AbstractSweetness> findSweet(List<AbstractSweetness> sweetList, double startPoint, double endPoint) {
		List<AbstractSweetness> range = new ArrayList<AbstractSweetness>();
		log.log(Level.DEBUG, "Start finding sweets, where sugar count between "+ startPoint+ " and "+endPoint);
		for (int i=0; i<sweetList.size(); i++){
			if(startPoint < sweetList.get(i).getSugarCont() && sweetList.get(i).getSugarCont()< endPoint){
				range.add(sweetList.get(i));
				
			}
		
		}
		log.log(Level.INFO,"Count of suitable sweets is "+range.size());
		return range;
		
	}
	
		
}


