package com.epam.training.action;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;



public class DataReaderFromFile {
	
	static Logger log = LogManager.getLogger("DataReaderFromFile");

	
	public static List<ArrayList<String>> readFile(){
		
		final File file = new File("data/temp.txt");
		
		
		// This will reference one line at a time

		List<ArrayList<String>> listLines = new ArrayList<ArrayList<String>>();

		try {
			// FileReader reads text files in the default encoding.
			FileReader fileReader = new FileReader(file);
			// Wrap FileReader in BufferedReader.
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			
			
			String line = null;
			
            while(( line= bufferedReader.readLine() ) != null){
            	
            	String[] lineStr = line.split("-");
            	ArrayList<String> lineArray =  new ArrayList<String>( Arrays.asList(lineStr));
            	
            	listLines.add(lineArray);
            	
            	}
            
      		// Close file.
			bufferedReader.close();   
			log.log(Level.INFO,"File reading is successfully completing");
			
			
		}
		catch(FileNotFoundException ex) {
			System.err.println(
            "Unable to open file '" + 
            		file + "'");  
			 log.log(Level.ERROR,"Can't open file: ", ex);
		}
		catch(IOException ex) {
			System.err.println(
            "Error reading file '" 
            + file + "'");  
			log.log(Level.ERROR,"Can't read file: ", ex);
        
		}
		return listLines;

	}
	
	
}
