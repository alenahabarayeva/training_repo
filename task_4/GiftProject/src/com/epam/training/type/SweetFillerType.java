package com.epam.training.type;

public enum SweetFillerType{
	MILKCHOCOLATE, DARKCHOCOLATE, CARAMEL, GLAZECARAMEL, CONDENSEDMILK;
	public static SweetFillerType convert(String str) {
        for (SweetFillerType sweetType : SweetFillerType.values()) {
            if (sweetType.toString().equals(str)) {
                return sweetType;
            }
        }
        return null;
    }
}

