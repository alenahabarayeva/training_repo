package com.epam.training.type;

public enum SweetType {
	CN_NUT, CN_CR_CHOCO, CR_HERBINA, CR_DUSHESS, CH_SLIVKI, CH_ELEGANCE, WF_ROSHETTO, WF_KONAFETTO;
	
	public static SweetType getRandom(){
		return values()[(int) (Math.random() *values().length)];
	}

}
