package com.epam.training.type;

public enum CandyMass {
	FONDANT, MARZIPAN, MILK;
	
	public static CandyMass getRandom(){
		return values()[(int) (Math.random() *values().length)];
	}
	
	public static CandyMass convert (String str){
		for (CandyMass canMass : CandyMass.values()) {
            if (canMass.toString().equals(str)) {
                return canMass;
            }
        }
        return null;
    }
	
}


