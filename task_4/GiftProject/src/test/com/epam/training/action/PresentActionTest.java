package test.com.epam.training.action;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.epam.training.creator.CandyCreatorFromFile;

import static com.epam.training.action.DataReaderFromFile.readFile;
import static com.epam.training.action.PresentAction.countPresentWeight;
import static com.epam.training.action.PresentAction.findSweet;
import static org.junit.Assert.assertEquals;

public class PresentActionTest {
	
	private String content_pres = "Roshen - 12 - 11 - MILKCHOCOLATE-MILK"+"\n"+"Roshen_test - 12 - 11 - DARKCHOCOLATE-FONDANT";
	private List<ArrayList<String>> testList = readFile();
	
	@Before
	public void writeData (){
		try (BufferedWriter bw = new BufferedWriter(new FileWriter("data/temp.txt"))) {
			bw.write(content_pres);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
		
	@Test
	public void countPresentWeightTest(){
		CandyCreatorFromFile listAbstr = new CandyCreatorFromFile();
		double actual = countPresentWeight(listAbstr.createCandy(this.testList));
		assertEquals(actual, 24, 0.01);
		
	}


	@Test
	public void findSweetTest(){
		CandyCreatorFromFile listAbstr = new CandyCreatorFromFile();
		int actual = findSweet((listAbstr.createCandy(this.testList)), 10, 13).size();
		assertEquals(actual, 2, 0.01);
	}
}
