package test.com.epam.training.action;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static com.epam.training.action.DataReaderFromFile.readFile;

import static org.junit.Assert.*;

public class DataReaderFromFileTest {
	
	private BufferedReader in = null;
	private String content = "Roshen - 12 - 11 - MILKCHOCOLATE-MILK";
	
	
	@Before
	public void writeData (){
		try (BufferedWriter bw = new BufferedWriter(new FileWriter("data/temp.txt"))) {
			bw.write(content);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	@Before
	public void openBuff() throws IOException{
		in = new BufferedReader(new FileReader("data/temp.txt"));
	}
	
	@After
	public void closeBuff() throws IOException{
		if( in != null){
			in.close();
		}
		in = null;
	}
	
	@Test
	public void testFile () throws IOException {
		
		String line = in.readLine();
		assertNotNull(line);
	}
	
	@Test
	public void readFileTest (){
		List<ArrayList<String>> testList = readFile();
		List<String> constr = new ArrayList<String> ();
		for (int i = 0; i<testList.size(); i++){
			constr.addAll( (testList.get(i)));
		}
		
		List<String> stringList = Arrays.asList(content.split("-"));
		Set<String> intersect = new HashSet<String>(constr);
	    intersect.retainAll(stringList);
	    
	    Assert.assertEquals(constr.size(),intersect.size());
	}
	 
	
}
