package test.com.epam.training.formula;

import org.junit.Test; 
import org.junit.runner.RunWith; 
import org.junit.runners.Parameterized; 


import static org.junit.Assert.*;
import com.epam.training.formula.ComposFormula;
import java.util.Arrays;
import java.util.Collection; 
import org.junit.AfterClass;
@RunWith(Parameterized.class) 


public class ComposFormulaTest {

	private int num;
	private int comp;
	private static int count = 0;
	
	
	//public costruct
	
	public ComposFormulaTest (int num, int comp){
		
		this.num = num;
		this.comp = comp;
	}


	@Parameterized.Parameters
	public static Collection<Object[]> compFormulaValues(){
		return Arrays.asList(new Object[][] {
			{1234, 24},
			{1111, 1},
			{1121, 2}
		});
	
	}	
	
	@Test
	public void composFormulaTest(){
		int expected = this.comp;
		int actual = ComposFormula.countComposition(this.num);
		assertEquals(expected, actual, 0.01);
		count++;
	}
	
	@Test( timeout = 1)
		public void composFormulaTest2(){
			int expected = this.comp;
			int actual = ComposFormula.countComposition(this.num);
			assertEquals(expected, actual, 0.01);
		count++;
	}
	
	
	@AfterClass
    public static void afterClass(){
    System.out.println("Count of tests: " + count);
 }

}