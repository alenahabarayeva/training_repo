package com.epam.training.main;

import com.epam.training.report.ReportComposition;

public class Main {

	public static void main(String[] args) {
		
		ReportComposition drc = new ReportComposition("Test report");
		System.out.println("Report name: "+ drc.getRepName());
		drc.generateReport();
	}

}
