package com.epam.training.report;

import java.util.Scanner;
import com.epam.training.datareader.DataReaderFromConsole;
import com.epam.training.datareader.DataReaderFromFile;
import com.epam.training.validation.InputValidation;
import com.epam.training.formula.ComposFormula;
import com.epam.training.datawriter.DataWriterToFile;

public class ReportComposition {
	
	private String repName;
	
	public ReportComposition(){		
		repName = "Ad hoc report";		
	};
	
	public ReportComposition(String name){	
		repName = name;	
	};
	
	public String getRepName(){
		return repName;
	}
	
	private String getSourceType (){
		String sourceType = null;
		Scanner sc = new Scanner(System.in);
		System.out.println("Entered data source (console or file):");
		
		sourceType = sc.next();
				
		return sourceType;
	}
	
	public void generateReport(){
		
		String st = getSourceType();
		int a = 0;
		
		switch (st){
		case "console" :
			DataReaderFromConsole  drc = new DataReaderFromConsole();
			a = drc.getData();
		break;
		case "file":
			a = DataReaderFromFile.readFile();
		break;		
		}
		
		if (a > 0){
			int n = 4;
			InputValidation val = new InputValidation(n);
			if (val.checkInput(a) > 0){
				int comp = ComposFormula.countComposition (a);
				System.out.println("Number composition is equil " + comp);
				DataWriterToFile dwf = new DataWriterToFile();
				dwf.writeData(a, comp);
				System.out.println("Data was written into file. See tempWriter.txt file.");
			}
			else{
				System.out.println("You entered no " + n + "-digit number. Please retype or check file.");
			}
			
		}
		else{
			System.out.println("You entered inappropriate data. Please retype or check file.");
		}
	
	}

}
