package com.epam.training.datawriter;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;


public class DataWriterToFile {

	private static final String FILENAME = "data/tempWriter.txt";
	
	public void writeData (int num, int comp){
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(FILENAME))) {

			String content = "You entered number "+ num + ". Composition of this number is equal "+ comp;

			bw.write(content);


		} catch (IOException e) {

			e.printStackTrace();

		}

	}
	
}
