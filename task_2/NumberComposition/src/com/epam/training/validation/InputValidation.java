package com.epam.training.validation;

public class InputValidation {
	
	private int numBit;
	
	public InputValidation(int numb){
		numBit = numb;
	}
	
	
	public int checkInput (int num) {
		// start check count digits of number
		int cnt = (int) Math.log10(num)+1;
		if (cnt == numBit) { 
		//	System.out.println("You entered right number");
			num = cnt;
		}
		else{
			 num = 0;
		}
		return num;
	}

}
