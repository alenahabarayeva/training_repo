package com.epam.training.datareader;

import java.util.Scanner;

public class DataReaderFromConsole {

	 public int getData () {
		
		 Scanner scan = new Scanner(System.in);
		 int num = 0;
		
		 System.out.println("Enter a four digit number: ");

		
		if (scan.hasNextInt()) {
	
			num = Math.abs(scan.nextInt());
		}
		else {
			num = 0;
		}
		scan.close();
		return num;
					
	}
	
}
