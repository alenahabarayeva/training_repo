package com.epam.training.datareader;

import java.io.*;

public class DataReaderFromFile {
	
	public static int readFile(){
		
		final File file = new File("data/temp.txt");
		
		
		// This will reference one line at a time
		String line = null;
		int num = 0;
		try {
			// FileReader reads text files in the default encoding.
			FileReader fileReader = new FileReader(file);
			// Wrap FileReader in BufferedReader.
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			
            line = bufferedReader.readLine();
      		// Close file.
			bufferedReader.close();   
			
			if(line.matches("\\d*$")){
				//file contains only  digits
				num = Integer.parseInt(line);			
			}
		}
		catch(FileNotFoundException ex) {
			System.err.println(
            "Unable to open file '" + 
            		file + "'");                
		}
		catch(IOException ex) {
			System.err.println(
            "Error reading file '" 
            + file + "'");                  
        
    }
		return num;

	}


}
