package com.epam.training.calculation;

public class LengthCalculation {
	
	public static void circleLength(double rad) {
		
		double len = 2 * Math.PI * rad;
		System.out.println("Circle length has value, which is equal " + len);
		
	}

}
