package com.epam.training.validation;

import java.util.Scanner;


public class InputValidation {
	
	static public double checkInput () {
		
		System.out.print("Enter a radius: ");
		
		Scanner scan = new Scanner(System.in);
		double rad = scan.nextDouble();
		if (rad >= 0) {
			System.out.println("You entered right value for radius " + rad);
			}
		else {
			System.out.println("You entered negative number. Please retype.");
			rad = 0;
		}
			return rad;
					
	}
	
}
