package com.epam.training.entity;

import com.epam.training.validation.InputValidation;
import com.epam.training.calculation.LengthCalculation;
import com.epam.training.calculation.SquareCalculaion;


public class Circle {
	
	private double radius = InputValidation.checkInput();
	
	public void getLength() {
		if (radius != 0){
		LengthCalculation.circleLength(radius);
	    }
	}
		
	public void  getSquare() {
		if (radius != 0){
				SquareCalculaion.circleSquare(radius);
		}
	}
}
